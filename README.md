# Test Admission TUTEN

### Requisitos - Backend
- Se requiere Java 1.8 o superior.

### Instalacion y despliegue - Backend
```shell
cd test-admission-tuten/backend/test-admission
./mvnw clean install
./mvnw spring-boot:run
```

### Requisitos - Fronted
- Se require NPM v7.x o superior.

### Instalacion y despliegue - Backend
```shell
cd test-admission-tuten/frontend/test-admission-front
npm i
npm start
```



# Problema 1
Describe (lo más detalladamente posible) cómo crearías un stack Postgres/Java8/React. Se espera una descripción de protocolos y componentes.

Respuesta.
Inicialmente defino el scope y los goals del proyecto, desarrollando una arquitectura que me permite descompilar rápidamente entre una tecnología u otra, trabajar con una arquitectura en capas o microservicios son modelos que se ajustan a la necesidad rápidamente sin dejar de lado la cohesión y el bajo acoplamiento.

Una vez definido esto, los componentes requeridos para trabajar son:

Backend:
- Componente API Gateway - Utilizado para enrutar solicitudes, Otorgar alta disponibilidad.
- Componente Core Business - Utilizado para exponer funcionalidades concretas de negocio.
- Componente Core Data - Utilizado exclusivamente para acceder a bases de datos y efectuar operaciones sobre ella.
- Componente Core Security - Utilizado para Autenticar, Autorizar y validar aspectos de acceso y permanencia en los diferentes componentes.

Frontend:
- Componente Login / SignIn - Utilizado exclusivamente para autenticar al usuario, embeber el acceso en otro site o aplicación y dar acceso a los componentes principales.
- Componente Core Home - Utilizado para definir las diferentes funcionalidades a las que tiene acceso el usuario autorizado y redirecciona a la versión concreta del micro frontend.


# Problema 2
Servicios expuestos:
![alt text](./docs/service_time_convert.png "Service Exposed")
Swagger documentation: http://localhost:9090/test-admission/api/v2/api-docs

Evidencia prueba - junit:
![alt text](./docs/test_service_time_convert.png "Success Testing")


# Problema 3
- http://localhost:3000/login
![alt text](./docs/screenshots/login.png "LogIn")


- http://localhost:3000/booking
![alt text](./docs/screenshots/all_bookings.png "Find Bookings")
