import React, {useEffect, useState} from 'react';
import {TableUI} from '../../components/TableUI';
import {getAllBookingsAction} from '../../actions/bookingAction';
import {useDispatch, useSelector} from 'react-redux';


const BookingComponent: React.FC = () => {

    const dispatch = useDispatch();

    const email = useSelector((state: any) => state.authentication.email);

    const columns: string[] = ['Booking Id', 'Cliente', 'Fecha de Creación', 'Dirección', 'Precio'];
    const [rows, setRows] = useState([]);
    useEffect(() => {
        (async () => {
            const result: any = await dispatch(getAllBookingsAction(email));
            setRows(result);
        })();
    }, []);

    return (
        <div>
            <TableUI labels={columns} data={rows}/>
        </div>
    );
};

export default BookingComponent;
