import React, {useState} from 'react';

import './LoginComponent.css';
import {loginAction} from '../../actions/loginAction';
import InputUI from '../../components/InputUI';
import {useDispatch} from 'react-redux';
import {useAuthHook} from './../../components/context/provider/UtilsProvider';
import {Redirect} from 'react-router';


const LoginComponent: React.FC = () => {

    const dispatch = useDispatch();

    const {authenticated} = useAuthHook();

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    if (authenticated)
        return <Redirect to={'/booking'}/>;

    const submitHandler = (event: { preventDefault: () => void; }) => {
        dispatch(loginAction(email, password));
        event.preventDefault();
    };

    function validationForm() {
        return email.length > 0 && password.length > 0;
    }

    return (
        <div>
            <form className="form-sign-in" onSubmit={submitHandler}>
                <InputUI label={'Email address'} input={{
                    type: 'email',
                    placeholder: 'Email address',
                    object: {value: email, action: setEmail},
                }} focus={true}/>
                <InputUI label={'Password'} input={{
                    type: 'password',
                    placeholder: 'Password',
                    object: {value: password, action: setPassword},
                }}/>
                <button className="btn btn-lg btn-primary btn-block" type="submit" disabled={!validationForm()}>
                    Log In
                </button>
            </form>
        </div>
    );
};

export default LoginComponent;
