import React from 'react';
import {TableFormType} from '../data/models/components/TableType';


export const TableUI: React.FC<TableFormType<any>> = (table) => {
    const headers: string[] = table.labels;
    const rows: any[] = table.data;
    return (
        <div>
            <table className="table table-hover">
                <thead>
                <tr>
                    {
                        headers && headers.map((element, index) => {
                            return <th scope="col" key={index}>{element}</th>;
                        })
                    }
                </tr>
                </thead>
                <tbody>
                {
                    rows && rows.map((element, index) => {
                        return (
                            <tr className={index % 2 === 0 ? 'table-dark' : 'table-light'} key={element.bookingId}>
                                <td>{element.bookingId}</td>
                                <td>{element.tutenUserClient.lastName} {element.tutenUserClient.firstName}</td>
                                <td>{element.bookingTime}</td>
                                <td>{element.locationId.streetAddress}</td>
                                <td>{element.bookingPrice}</td>
                            </tr>
                        );
                    })
                }
                </tbody>
            </table>
        </div>
    );
};
