import React, {useState} from 'react';
import {FieldFormType} from '../data/models/components/FieldFormType';
import {validateEmail} from '../utilities/ValidationUtility';


const InputUI: React.FC<FieldFormType<any>> = (field) => {
    const [valueState, eventState] = [field.input.object.value, field.input.object.action];
    const [tagsCSS, setTagsCSS] = useState({parent: '', children: ''});

    const validateField = (value: string, type: string) => {
        let isValid: boolean = false;
        switch (type) {
            case 'email': {
                isValid = validateEmail(value);
                break;
            }
            case 'password': {
                isValid = (value && value.length >= 3) || false;
                break;
            }
            default:
                break;
        }

        if (value)
            setTagsCSS({parent: isValid ? 'has-success' : 'has-danger', children: isValid ? 'is-valid' : 'is-invalid'});
    };

    return (
        <div className={'form-group ' + tagsCSS.parent}>
            <label className="sr-only form-control-label" htmlFor="inputValid">{field.label}</label>
            <input className={'form-control ' + tagsCSS.children} type={field.input.type}
                   placeholder={field.input.placeholder} value={valueState} autoFocus={field.focus} required
                   onChange={event => {
                       eventState(event.target.value);
                   }}
                   onBlur={event => {
                       validateField(event.target.value, field.input.type);
                   }}
            />
        </div>
    );
};

export default InputUI;
