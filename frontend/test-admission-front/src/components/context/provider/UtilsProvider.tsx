import React, {useContext} from 'react';
import {useSelector} from 'react-redux';
import {AuthContext} from '../ContextApp';


export const UtilsProvider: React.FC = ({children}) => {
    const isAuth = useSelector((state: any) => state.authentication.isAuthenticated);
    return (
        <AuthContext.Provider value={{authenticated: isAuth}}>
            {children}
        </AuthContext.Provider>
    );
};

export const useAuthHook = () => {
    return useContext(AuthContext);
};

