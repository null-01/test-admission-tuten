import httpClientRestful from '../utilities/HttpClientUtility';
import {LOAD_BOOKING_ERROR, LOAD_BOOKING_OK, LOADING_END, LOADING_START} from './types';
import {Dispatch} from 'react';

export const getAllBookingsAction = (email: string) => {
    return async (dispatch: Dispatch<any>) => {
        dispatch({type: LOADING_START});
        return httpClientRestful.get(`/user/${email}/bookings?current=true`).then(success => {
            dispatch({type: LOAD_BOOKING_OK, payload: success.data});
            return success.data;
        }).catch(error => {
            dispatch({type: LOAD_BOOKING_ERROR, payload: error});
            return null;
        }).finally(() => {
            dispatch({type: LOADING_END});
        });
    };
};
