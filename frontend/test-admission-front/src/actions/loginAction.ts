import httpClientRestful from '../utilities/HttpClientUtility';
import {LOADING_END, LOADING_START, LOGIN_FAILED, LOGIN_SUCCESS} from './types';
import {Dispatch} from 'react';

export const loginAction = (email: string, password: string) => {
    return (dispatch: Dispatch<any>) => {
        dispatch({type: LOADING_START});
        const headers = {'password': password};
        httpClientRestful.post(`/user/${email}`, '{}', headers).then(success => {
            dispatch({type: LOGIN_SUCCESS, payload: success.data});
        }).catch(error => {
            dispatch({type: LOGIN_FAILED, payload: error});
        }).finally(() => {
            dispatch({type: LOADING_END});
        });
    };
};
