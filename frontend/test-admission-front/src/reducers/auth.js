import {LOGIN_FAILED, LOGIN_SUCCESS} from "../actions/types";

const initialStateAuthentication = {
    token: localStorage.getItem("access_token"),
    isAuthenticated: false,
    email: '',
    error: {}
};

export const authentication = (state = initialStateAuthentication, action) => {
    const {type, payload} = action;
    switch (type) {
        case LOGIN_SUCCESS:
            localStorage.setItem("access_token", payload.sessionTokenBck);
            return {
                ...state,
                email: payload.email,
                token: payload.sessionTokenBck,
                isAuthenticated: true
            };
        case LOGIN_FAILED:
            return {
                ...state,
                isAuthenticated: false,
                error: {
                    message: payload
                }
            }
        default:
            return state;
    }
}
