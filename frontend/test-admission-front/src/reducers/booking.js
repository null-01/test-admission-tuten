import {LOAD_BOOKING_ERROR, LOAD_BOOKING_OK} from "../actions/types";

const initialStateBooking = {
    list: [],
    error: {}
};

export const booking = (state = initialStateBooking, action) => {
    const {type, payload} = action;
    switch (type) {
        case LOAD_BOOKING_OK:
            return {
                ...state,
                list: payload
            };
        case LOAD_BOOKING_ERROR:
            return {
                ...state,
                error: {
                    message: payload
                }
            }
        default:
            return state;
    }
}
