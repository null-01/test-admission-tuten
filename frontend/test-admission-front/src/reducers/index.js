import {combineReducers} from 'redux';
import {authentication} from './auth';
import {booking} from './booking';
import {common} from './common';

export default combineReducers({
    authentication,
    common,
    booking,
});
