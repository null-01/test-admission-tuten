import {LOADING_END, LOADING_START} from "../actions/types";

const initialStateCommon = {
    loading: false,
    error: {}
};

export const common = (state = initialStateCommon, action) => {
    const {type} = action;
    switch (type) {
        case LOADING_START:
            return {
                ...state,
                loading: true,
            };
        case LOADING_END:
            return {
                ...state,
                loading: false,
            }
        default:
            return state;
    }
}
