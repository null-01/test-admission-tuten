import {Dispatch, SetStateAction} from 'react';

export interface FieldFormType<S> {
    focus?: boolean,
    label: string,
    input: {
        type: 'email' | 'password'
        placeholder?: string,
        object: {
            value: S,
            action: Dispatch<SetStateAction<S>>
        }
    }
}
