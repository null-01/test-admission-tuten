export interface TableFormType<S> {
    labels: string[],
    data: any[]
}
