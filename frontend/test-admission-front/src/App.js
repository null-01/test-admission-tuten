import React, {Fragment} from 'react';

import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import {Provider} from 'react-redux';
import store from './store';
import LoginComponent from './routes/login/LoginComponent';
import {UtilsProvider} from "./components/context/provider/UtilsProvider";
import BookingComponent from "./routes/booking/BookingComponent";

function App() {
    return (
        <Provider store={store}>
            <UtilsProvider>
                <Router>
                    <Fragment>
                        <Switch>
                            <Route exact path={['/', '/login']} component={LoginComponent}/>
                            <Route exact path={['/booking']} component={BookingComponent}/>
                        </Switch>
                    </Fragment>
                </Router>
            </UtilsProvider>
        </Provider>
    );
};

export default App;
