import axios from 'axios';

// Need define API URL in constants or .env
// const API_URL = 'https://dev.tuten.cl/TutenREST/rest';
const API_URL = 'https://77fa7088-af84-490b-9874-36ada8d1c8c5.mock.pstmn.io/TutenREST/rest';


const getHeaders = (headers?: any) => {
    return {
        'Content-Type': 'application/json',
        'Accept': '*/*',
        'app': 'APP_BCK',
        'token': localStorage.getItem('access_token'),
        ...headers
    };
};

const getInstance = () => {
    return axios.create({
        baseURL: API_URL,
    });
};

async function get(url: string) {
    return getInstance()({
        url,
        method: 'GET',
        headers: getHeaders(),
    });
};

export async function post(url: string, body: any, headers?: any) {
    return getInstance()({
        url,
        method: 'POST',
        data: body,
        headers: getHeaders(headers),
    });
};

export default {get, post};
