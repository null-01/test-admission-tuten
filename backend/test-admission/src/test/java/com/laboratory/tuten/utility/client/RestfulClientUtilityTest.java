package com.laboratory.tuten.utility.client;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;


@SpringBootTest
@AutoConfigureMockMvc
public class RestfulClientUtilityTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    @DisplayName("CONSUME SERVICE RESTFUL - SUCCESS")
    public void successConsumeServiceRestful() throws Exception {

        MvcResult result = mockMvc.perform(
                MockMvcRequestBuilders.post("/time/convert/utc")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n \"time\": \"18:31:00\", \"offset\": \"-03:00\"}")
        ).andExpect(MockMvcResultMatchers.status().isOk()).andReturn();

        String response = result.getResponse().getContentAsString();
        Assertions.assertEquals("{\"response\":{\"time\":\"20:31\",\"offset\":\"-03:00\",\"timezone\":\"UTC\"}}", response);
    }

    @Test
    @DisplayName("CONSUME SERVICE RESTFUL - FAILED")
    public void failedConsumeServiceRestful() throws Exception {

        MvcResult result = mockMvc.perform(
                MockMvcRequestBuilders.post("/time/convert/utc")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n \"time\": \"18:31:00\", \"offset\": \"-23:55\"}")
        ).andExpect(MockMvcResultMatchers.status().isOk()).andReturn();

        String response = result.getResponse().getContentAsString();
        Assertions.assertEquals("{\"response\":\"Zone offset hours not in valid range: value -23 is not in the range -18 to 18\"}", response);
    }

}
