/**
 *
 */
package com.laboratory.tuten.controller;

import com.laboratory.tuten.constant.TimeZoneEnum;
import com.laboratory.tuten.controller.dto.TimeZoneRequest;
import com.laboratory.tuten.controller.dto.TimeZoneResponse;
import com.laboratory.tuten.exception.CommonException;
import com.laboratory.tuten.exception.UtilityException;
import com.laboratory.tuten.technical.ResponseTechnical;
import com.laboratory.tuten.utility.ITimeZoneUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.ZonedDateTime;

/**
 * @author andresduran0205
 *
 */

@RestController
@RequestMapping("/time")
public class TimeZoneController {

    @Autowired
    private ITimeZoneUtility iTimeZoneUtility;

    @PostMapping(value = "/convert/{timezone:\\butc\\b}")
    public ResponseEntity<ResponseTechnical<TimeZoneResponse>> convertTimezone(
            @PathVariable(required = true) String timezone, @Valid @RequestBody TimeZoneRequest request)
            throws CommonException.DATA_CONVERT_NOT_WORK, UtilityException.OFFSET_FORMAT {

        iTimeZoneUtility.validateOffsetFormat(request.getOffset());
        TimeZoneEnum timeZoneEnum = TimeZoneEnum.valueOf(timezone.toUpperCase());
        switch (timeZoneEnum) {
            case UTC:
                ZonedDateTime time = iTimeZoneUtility.convertToTimeZoneUTC(request.getTime(), request.getOffset())
                        .orElseThrow(CommonException.DATA_CONVERT_NOT_WORK::new);

                TimeZoneResponse response = TimeZoneResponse.builder()
                        .time(time.getHour() + ":" + time.getMinute())
                        .offset(time.getOffset().toString())
                        .timezone(TimeZoneEnum.UTC.name()).build();

                return new ResponseEntity<ResponseTechnical<TimeZoneResponse>>(new ResponseTechnical<>(response), HttpStatus.OK);
            default:
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }
}

