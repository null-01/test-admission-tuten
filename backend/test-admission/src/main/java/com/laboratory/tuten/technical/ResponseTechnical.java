/**
 *
 */
package com.laboratory.tuten.technical;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author andresduran0205
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResponseTechnical<T> {
    private T response;
}
