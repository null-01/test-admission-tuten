/**
 *
 */
package com.laboratory.tuten.controller;

import com.laboratory.tuten.technical.ResponseTechnical;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * @author andresduran0205
 *
 */
@Slf4j
@RestControllerAdvice
public class HandlerExceptionController extends ResponseEntityExceptionHandler {

    @ExceptionHandler({Exception.class})
    public final ResponseEntity<ResponseTechnical> handleAllExceptions(Exception ex, WebRequest request) {
        Throwable cause = findCauseException(ex);
        cause = cause == null ? ex : cause;
        ResponseTechnical header = new ResponseTechnical(cause.getMessage());
        return new ResponseEntity<ResponseTechnical>(header, HttpStatus.OK);
    }

    @Override
    protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {
        ResponseTechnical header = new ResponseTechnical(ex.getMessage());
        return new ResponseEntity<Object>(header, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private Throwable findCauseException(Throwable ex) {
        log.error(ex.getMessage());
        if (ex.getCause() == null)
            return ex;
        return findCauseException(ex.getCause());
    }

}
