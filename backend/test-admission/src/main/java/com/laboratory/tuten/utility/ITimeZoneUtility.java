package com.laboratory.tuten.utility;

import com.laboratory.tuten.exception.UtilityException;

import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.util.Optional;

public interface ITimeZoneUtility {

    Optional<ZonedDateTime> convertToTimeZoneUTC(LocalTime time, String offset);

    void validateOffsetFormat(String offset) throws UtilityException.OFFSET_FORMAT;
}
