package com.laboratory.tuten.controller.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TimeZoneResponse {
    private String time;
    private String offset;
    private String timezone;
}
