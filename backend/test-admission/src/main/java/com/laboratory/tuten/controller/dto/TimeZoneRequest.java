package com.laboratory.tuten.controller.dto;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.time.LocalTime;

@Data
@Builder
public class TimeZoneRequest {
    @NotNull
    private LocalTime time;
    @NotNull
    private String offset;
}
