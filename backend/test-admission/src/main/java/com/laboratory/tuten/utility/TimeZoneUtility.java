package com.laboratory.tuten.utility;

import com.laboratory.tuten.exception.UtilityException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.time.*;
import java.util.Optional;
import java.util.regex.Pattern;

@Slf4j
@Component
public class TimeZoneUtility implements ITimeZoneUtility {

    public Optional<ZonedDateTime> convertToTimeZoneUTC(LocalTime localTime, String offset) {
        LocalDateTime localDateTime = LocalDateTime.of(LocalDate.now(), localTime);
        ZonedDateTime currentZonedDateTime = ZonedDateTime.of(localDateTime, ZoneId.systemDefault());
        ZoneOffset zoneOffset = ZoneOffset.of(offset);
        return Optional.of(currentZonedDateTime.withZoneSameInstant(zoneOffset));
    }

    public void validateOffsetFormat(String offset) throws UtilityException.OFFSET_FORMAT {
        if (!Pattern.compile("^[+-]\\d{2}:\\d{2}$").matcher(offset).matches())
            throw new UtilityException.OFFSET_FORMAT();
    }

}
