/**
 * 
 */
package com.laboratory.tuten.exception;

/**
 * @author andresduran0205
 *
 */


public class CommonException {


    public static class DATA_CONVERT_NOT_WORK extends Exception {

	}

	public static class DATA_NOT_SAVED extends Exception {

	}

	public static class DATA_FORMAT_INVALID extends Exception {

	}

	
}
