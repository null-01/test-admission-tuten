/**
 *
 */
package com.laboratory.tuten.exception;

/**
 * @author andresduran0205
 *
 */


public class UtilityException {

    public static class OFFSET_FORMAT extends Exception {
        public OFFSET_FORMAT() {
            super("Parameter offset is invalid - Example valid format +03:00");
        }
    }

}
